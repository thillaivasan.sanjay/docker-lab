# docker-lab

## Code sharing during sessions
https://notepad.pw/docker-lab

## Video recordings
- [Jul 18 2020](https://it-video-recording.s3.amazonaws.com/docker-training-pradeep/01-jul-18-2020.mp4) - Basics of Docker and Docker commands
- [Jul 19 2020](https://it-video-recording.s3.amazonaws.com/docker-training-pradeep/02-jul-19-2020.mp4) - Docker commands deeper dive, Basics of Dockerfile and build
- [Jul 25 2020](https://it-video-recording.s3.amazonaws.com/docker-training-pradeep/03-jul-25-2020.mp4) - Dockerfile commands - Container running a script, container building Node.js and Spring apps
- [Jul 26 2020](https://it-video-recording.s3.amazonaws.com/docker-training-pradeep/04-jul-26-2020.mp4) - Dockerfile commands - Discussed questions regarding stopped and exited and restarting exited container, Multi-stage Dockerfile
- [Aug 1 2020](https://it-video-recording.s3.amazonaws.com/docker-training-pradeep/05-aug-1-2020.mp4) - Networking in Docker
- [Aug 2 2020](https://it-video-recording.s3.amazonaws.com/docker-training-pradeep/06-aug-2-2020.mp4) - Volumes in Docker
- [Aug 16 2020](https://it-video-recording.s3.amazonaws.com/docker-training-pradeep/07-aug-16-2020.mp4) - Docker Swarm and compose (??)

## References
- [Containers from Scratch by Liz Rice - explains exactly what a container is](https://www.youtube.com/watch?v=8fi7uSYlOdc&feature=youtu.be)
- [__A Beginner-Friendly Introduction to Containers, VMs and Docker__ by Preeti Kasireddy](https://medium.com/free-code-camp/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b)
- [__Lifecycle of Docker Container__ by Nitin AGARWAL](https://medium.com/@BeNitinAgarwal/lifecycle-of-docker-container-d2da9f85959)
- [Comparsion of container OSes](https://rancher.com/blog/2019/comparison-of-container-operating-systems/) - Good article on choosing the  different base OS, and the difference between busybox vs alpine